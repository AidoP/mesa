/*
 * Copyright (C) 2019 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __PANFROST_QUIRKS_H
#define __PANFROST_QUIRKS_H

/* Model-specific quirks requiring workarounds/etc. Quirks may be errata
 * requiring a workaround, or features. We're trying to be quirk-positive
 * here; quirky is the best! */

/* Whether the GPU lacks the capability for hierarchical tiling, without an
 * "Advanced Tiling Unit", instead requiring a single bin size for the entire
 * framebuffer be selected by the driver */

#define MIDGARD_NO_HIER_TILING (1 << 0)

/* bit 1-6 unused */

/* Whether this GPU lacks support for any typed loads, requiring packing */
#define MIDGARD_NO_TYPED_BLEND_LOADS (1 << 7)

/* bits 8-10 unused */

/* Does this GPU support anisotropic filtering? */
#define HAS_ANISOTROPIC (1 << 11)

static inline unsigned
panfrost_get_quirks(unsigned gpu_id, unsigned gpu_revision)
{
        switch (gpu_id) {
        case 0x600:
        case 0x620:
                return MIDGARD_NO_TYPED_BLEND_LOADS;

        case 0x720:
        case 0x820:
        case 0x830:
                return MIDGARD_NO_HIER_TILING;

        case 0x750:
        case 0x860:
        case 0x880:
                return 0;

        case 0x6000: /* G71 */
                return 0;

        case 0x6221: /* G72 */
                /* Anisotropic filtering is supported from r0p3 onwards */
                return (gpu_revision >= 0x30 ? HAS_ANISOTROPIC : 0);

        case 0x7093: /* G31 */
        case 0x7211: /* G76 */
        case 0x7212: /* G52 */
        case 0x7402: /* G52r1 */
                return HAS_ANISOTROPIC;

        default:
                unreachable("Unknown Panfrost GPU ID");
        }
}

#endif
